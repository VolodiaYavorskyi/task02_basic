package com.vova.task2;

import java.util.Scanner;

/**
 * A class that contains main method.
 * 
 * @author Volodymyr Yavorskyi
 * 
 */
public class App {
	/**
	 * Scans two integers from user to make an interval,
	 * then scans one integer n to make n Fibonacci numbers.
	 * 
	 * @param args
	 */
    public static void main(String[] args) {
    	Scanner scan = new Scanner(System.in);
    	
    	System.out.print("Enter an interval (two integers): ");
    	int from = scan.nextInt();
    	int to = scan.nextInt();
    	
    	if (to < from) {
    		int temp = to;
    		to = from;
    		from = temp;
    	}
    	
    	System.out.println("Odd numbers from start to end");
    	printOddFromStart(from, to);
    	
    	System.out.println("Even numbers from end to start");
    	printEvenFromEnd(from, to);
    	
    	System.out.println("Sum of all odd numbers: " + sumOfOdd(from, to));
    	System.out.println("Sum of all even numbers: " + sumOfEven(from, to));
    	
    	System.out.println();
    	System.out.println("Fibonacci numbers");
    	System.out.print("Enter n (>= 2): ");
    	int n = scan.nextInt();
    	scan.close();
    	
    	Fibonacci fib = new Fibonacci(n);
    	
    	System.out.println("Biggest odd number: " + fib.biggestOdd());
    	System.out.println("Biggest even number: " + fib.biggestEven());
    	System.out.println("Odd numbers percentage: " + fib.oddPercentage());
    	System.out.println("Even numbers percentage: " + fib.evenPercentage());
    }
    
    /**
     * Function that prints all odd numbers of an interval from start to end.
     * 
     * @param from start of an interval
     * @param to end of an interval
     */
    public static void printOddFromStart(int from, int to) {
    	if (from % 2 == 0) {
    		from++;
    	}
    	
    	for (int i = from; i <= to; i += 2) {
    		System.out.print(i + " ");
    	}
    	System.out.println();
    }
    
    /**
     * Function that prints all even numbers of an interval from end to start.
     * 
     * @param from start of an interval
     * @param to end of an interval
     */
    public static void printEvenFromEnd(int from, int to) {
    	if (to % 2 != 0) {
    		to--;
    	}
    	
    	for (int i = to; i >= from; i -= 2) {
    		System.out.print(i + " ");
    	}
    	System.out.println();
    }
    
    /**
     * Function that calculates sum of all odd numbers from an interval.
     * 
     * @param from start of an interval
     * @param to end of an interval
     * @return sum of odd numbers
     */
    public static long sumOfOdd(int from, int to) {
    	long res = 0;
    	
    	if (from % 2 == 0) {
    		from++;
    	}
    	
    	for (int i = from; i <= to; i += 2) {
    		res += i;
    	}
    	
    	return res;
    }
    
    /**
     * Function that calculates sum of all even numbers from an interval.
     * 
     * @param from start of an interval
     * @param to end of an interval
     * @return sum of even numbers
     */
    public static long sumOfEven(int from, int to) {
    	long res = 0;
    	
    	if (from % 2 != 0) {
    		from++;
    	}
    	
    	for (int i = from; i <= to; i += 2) {
    		res += i;
    	}
    	
    	return res;
    }
}
