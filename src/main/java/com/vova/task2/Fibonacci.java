package com.vova.task2;

/**
 * A class that contains array of Fibonacci numbers.
 * 
 * @author Volodymyr Yavorskyi
 *
 */
public class Fibonacci {
	/**
	 * Array that will contain Fibonacci numbers.
	 */
	private int[] arr;
	
	/**
	 * Size of the futule array.
	 */
	private final int n;
	
	/**
	 * To contain biggest odd number of Fibonacci sequence.
	 */
	private int F1;
	
	/**
	 * To contain biggest even number of Fibonacci sequence.
	 */
	private int F2;
	
	/**
	 * Constructor that generates n Fibonacci numbers.
	 * 
	 * @param n size of array
	 */
	public Fibonacci(int n) {
		this.n = n;
		this.arr = new int[n];
		this.F1 = 0;
		this.F2 = 0;
		this.generate();
	}
	
	/**
	 * Fills array with Fibonacci numbers.
	 */
	private void generate() {
		arr[0] = 0;
		arr[1] = 1;
		
		for (int i = 2; i < n; i++) {
			arr[i] = arr[i - 1] + arr[i - 2];
		}
		
		for (int i = n - 1; i >= 0; i--) {
			if (arr[i] % 2 != 0) {
				F1 = arr[i];
				break;
			}
		}
		
		for (int i = n - 1; i >= 0; i--) {
			if (arr[i] % 2 == 0) {
				F2 = arr[i];
				break;
			}
		}
	}
	
	/**
	 * 
	 * @return biggest odd Fibonacci number of our array.
	 */
	public int biggestOdd() {
		return F1;
	}
	
	/**
	 * 
	 * @return biggest even Fibonacci number of our array.
	 */
	public int biggestEven() {
		return F2;
	}
	
	/**
	 * 
	 * @return percentage of odd numbers in our array.
	 */
	public double oddPercentage() {
		int counter = 0;
		
		for (int i = 0; i < n; i++) {
			if (arr[i] % 2 != 0) {
				counter++;
			}
		}
		
		return 100.0 * counter / n;
	}
	
	/**
	 * 
	 * @return percentage of odd numbers in our array.
	 */
	public double evenPercentage() {
		int counter = 0;
		
		for (int i = 0; i < n; i++) {
			if (arr[i] % 2 == 0) {
				counter++;
			}
		}
		
		return 100.0 * counter / n;
	}
}
